<?php

	return array(
		'type' => array(
			'title' => '接口类型',
			'type'  => 'select',
			'options'=>array(
				'default' => '测试接口',
			),
			'value'  => 'default'
		),
		'username'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'账号',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
		'pass'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'密码',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
		'key'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'key',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		)
	);