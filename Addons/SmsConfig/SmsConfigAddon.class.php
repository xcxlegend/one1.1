<?php

namespace Addons\SmsConfig;
use Common\Controller\Addon;

/**
 * 图片上传插件
 * @author Legend <2014-6-13 14:42:45>
 */

	class SmsConfigAddon extends Addon{

		public $info = array(
			'name'=>'SmsConfig',
			'title'=>'短信服务器配置',
			'description'=>'短信服务器',
			'status'=>1,
			'author'=>'Legend',
			'version'=>'0.1'
		);

		public function install(){
			return true;
		}

		public function uninstall(){
			return true;
		}

		/**
		 * 编辑器挂载的后台文档模型文章内容钩子
		 * @param array('id'=>'对应的file域的id', 'return' => '返回数据填写的域', 'type'=>'id|path')
		 */
		public function app_begin($data){
			$this->assign('addons_data', $data);
			foreach ($this->getConfig() as $key => $value) {
				C('Sms_'.$key, $value);
			}
			$this->assign('addons_config', $this->getConfig());
		}



		
	}
