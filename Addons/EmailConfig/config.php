<?php

	return array(
		'from'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'发送者邮箱账号',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
		'pass'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'发送者邮箱密码',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
		'server'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'邮箱服务器',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
		'port'=>array(//配置在表单中的键名 ,这个会是config[title]
			'title'=>'邮箱服务器端口',//表单的文字
			'type'=>'text',		 //表单的类型：text、textarea、checkbox、radio、select等
			'value'=>'',			 //表单的默认值
		),
	);