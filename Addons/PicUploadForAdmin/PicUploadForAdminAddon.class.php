<?php

namespace Addons\PicUploadForAdmin;
use Common\Controller\Addon;

/**
 * 图片上传插件
 * @author Legend <2014-6-13 14:42:45>
 */

	class PicUploadForAdminAddon extends Addon{

		public $info = array(
			'name'=>'PicUploadForAdmin',
			'title'=>'后台图片上传',
			'description'=>'单图片上传',
			'status'=>1,
			'author'=>'Legend',
			'version'=>'0.1'
		);

		public function install(){
			return true;
		}

		public function uninstall(){
			return true;
		}

		/**
		 * 编辑器挂载的后台文档模型文章内容钩子
		 * @param array('id'=>'对应的file域的id', 'return' => '返回数据填写的域', 'type'=>'id|path')
		 */
		public function adminPicUpload($data){
			$data['type'] = $data['type'] ? $data['type'] : 'id';
			$this->assign('addons_data', $data);
			$this->assign('addons_config', $this->getConfig());
			$this->display('content');
		}



		
	}
